
import re, os, shutil, datetime, time, subprocess, argparse
from subprocess import Popen,PIPE,STDOUT,call 
from time import gmtime, strftime, localtime

import numpy
import matplotlib.pyplot as plt
import matplotlib.patches as patches
import matplotlib.path as path

def getXticks():
	return ["Traffic", "PeopleOnStreet", "Kimono", "ParkScene", "Cactus", "BQTerrace", "BasketballDrive", "RaceHorsesC", "BQMall", "PartyScene", "BasketballDrill", "RaceHorses", "BQSquare", "BlowingBubbles", "BasketballPass", "FourPeople", "Johnny", "KristenAndSara", "BasketballDrillText", "ChinaSpeed", "SlideEditing", "SlideShow"]

def calcStatisticsForEachVideo(videos1, videoNames, nthreads):
	print(nthreads+" Thread")
	for x in range(0, len(videos1)):
		print("Video: "+videoNames[x])
		print("mean: "+str(numpy.mean(videos1[x])))
		print("median: "+str(numpy.median(videos1[x])))
		print("std: "+str(numpy.std(videos1[x])))
		print("var: "+str(numpy.var(videos1[x], dtype=numpy.float64)))
		print("cv: "+str(numpy.std(videos1)/numpy.mean(videos1)))

def calcStatistics(videos1, nthreads):
	print("With "+nthreads+" Thread")
	print("mean: "+str(numpy.mean(videos1)))
	print("median: "+str(numpy.median(videos1)))
	print("std: "+str(numpy.std(videos1)))
	print("var: "+str(numpy.var(videos1)))
	print("cv: "+str(numpy.std(videos1)/numpy.mean(videos1)))

def speedUp(seqVals, parVals):
	speedUp = parVals[:]
	for x in xrange(0, len(parVals)):
		speedUp[x] = seqVals[x]/parVals[x]
	return speedUp

def averageSpeedUp(seqVals, parVals):
	spd = speedUp(seqVals, parVals)
	avg = 0
	for x in xrange(0, len(spd)):
		avg += spd[x]
	return avg/len(spd)


def extendList(videos1):
	videos11 = []
	for x in videos1:
		videos11.extend(x)
	return videos11

def drawGraphicsSingle(DataArray, xTicksNames, color, label, graphNameToSave):
	N = len(DataArray)
	print(N)
	position = numpy.arange(N)
	width = 0.5
	fig, ax = plt.subplots()#figsize=(9.5, 5))
	plt.xlim(0, N)
	plt.xticks(position + 0.5, xTicksNames, rotation=90, fontsize=10)
	rect = ax.bar(position + 0.25, DataArray, width, bottom=0)
	# plt.title('Execution time per video with 1, 2, 3 and 4 threads')
	plt.ylabel('time (s)')
	plt.xlabel('video')
	plt.savefig(graphNameToSave+'.pdf', bbox_inches='tight', transparent=True, orientation='landscape')
	plt.close()

def drawGraphics(DataArray, xTicksNames, label, colors, graphNameToSave, xlabel, ylabel):
	N = len(DataArray[0])
	print(N)
	position = numpy.arange(N)
	width = 0.4
	fig, ax = plt.subplots()#figsize=(9.5, 5))
	plt.xlim(-1, 2*N+1)
	posXticks = []
	for x in range(0, len(position)):
		position[x] = 2 * position[x]
		posXticks.append(position[x] + 1)

	plt.xticks(posXticks, xTicksNames, rotation=90, fontsize=10)
	rect = [[], [], [], [], []]
	for x in xrange(0,len(DataArray)):
		rect[x] = ax.bar(position + (x*width), DataArray[x], width, bottom=0,  color=colors[x])
	plt.legend(rect, label, ncol=len(label), loc='upper center', prop={'size': 10},  bbox_to_anchor=(0.5, 1.1))
	# plt.legend(rect, label, ncol=1, loc='upper right', prop={'size': 10})
	
	# plt.title('Execution time per video with 1, 2, 3 and 4 threads')
	plt.ylabel(ylabel)
	plt.xlabel(xlabel)
	plt.savefig(graphNameToSave+'.pdf', bbox_inches='tight', transparent=True, orientation='landscape')
	plt.close()

def getXticks():
	return  ["Traffic", "PeopleOnStreet", "Kimono", "ParkScene", "Cactus", "BQTerrace", "BasketballDrive", "RaceHorsesC", "BQMall", "PartyScene", "BasketballDrill", "RaceHorses", "BQSquare", "BlowingBubbles", "BasketballPass", "FourPeople", "Johnny", "KristenAndSara", "BasketballDrillText", "ChinaSpeed", "SlideEditing", "SlideShow"]

videoNames = ["Basketball", "BQsquare", "RaceHorses", "BlowingBubbles"]

# Result for 10 executions of 4 videos
# sequential results
Basketballseq = [942.55, 939.83, 970.85, 957.04, 970.93, 942.66, 940.14, 971.12, 957.05, 939.91]
BQsquareseq = [960.08, 959.71, 960.44, 959.69, 959.79, 991.20, 959.40, 960.42, 960.51, 959.51]
RaceHorsesseq = [1034.13, 1035.21, 1034.04, 1034.71, 1051.89, 1037.24, 1041.38, 1034.49, 1034.15, 1052.03]
BlowingBubblesseq = [984.68, 999.09, 971.62, 967.98, 967.71, 985.10, 969.23, 999.34, 968.85, 999.33]
videosseq = [Basketballseq, BQsquareseq, RaceHorsesseq, BlowingBubblesseq]

# 1thread
Basketball1 = [939.69, 939.87, 956.69, 939.84, 940.26, 971.26, 971.19, 941.69, 939.76, 940.38]
BQsquare1 = [976.51, 964.42, 990.70, 976.56, 960.15, 959.37, 959.24, 959.40, 991.12, 960.21]
RaceHorses1 = [1067.17, 1034.88, 1067.27, 1034.72, 1034.63, 1066.95, 1034.33, 1035.10, 1052.01, 1051.88]
BlowingBubbles1 = [970.36, 999.05, 969.62, 969.02, 967.56, 998.96, 967.46, 999.31, 967.50, 998.85]
videos1 = [Basketball1, BQsquare1, RaceHorses1, BlowingBubbles1]

# 2 threads
Basketball2 = [727.66, 714.66, 727.67, 727.87, 727.49, 725.67, 725.66, 714.83, 727.65, 727.38]
BQsquare2 = [743.58, 741.83, 743.43, 730.49, 730.44, 741.78, 730.84, 730.53, 730.55, 730.38]
RaceHorses2 = [803.81, 789.88, 801.78, 803.71, 804.30, 801.67, 790.27, 801.53, 803.63, 790.76]
BlowingBubbles2 = [738.66, 749.72, 738.58, 738.99, 751.99, 752.16, 752.13, 751.98, 738.98, 738.86]
videos2 = [Basketball2, BQsquare2, RaceHorses2, BlowingBubbles2]

# 3 threads
Basketball3 = [515.21, 506.19, 514.55, 514.45, 506.66, 514.66, 506.99, 514.15, 514.74, 506.67]
BQsquare3 = [518.82, 527.26, 527.42, 518.23, 518.94, 525.07, 527.72, 518.59, 525.61, 518.85]
RaceHorses3 = [565.89, 565.32, 565.41, 572.22, 573.81, 574.92, 564.84, 574.81, 572.40, 565.91]
BlowingBubbles3 = [526.46, 535.76, 526.37, 534.84, 526.41, 535.57, 534.67, 526.62, 526.13, 533.49]
videos3 = [Basketball3, BQsquare3, RaceHorses3, BlowingBubbles3]

# 4 threads
Basketball4 = [407.19, 410.47, 400.43, 414.89, 411.62, 407.89, 408.60, 417.35, 410.24, 409.99]
BQsquare4 = [424.82, 410.97, 421.65, 415.95, 420.93, 416.72, 417.01, 411.59, 412.98, 420.49]
RaceHorses4 = [451.24, 457.18, 459.87, 469.11, 454.72, 464.57, 458.05, 454.75, 457.10, 456.68]
BlowingBubbles4 = [427.41, 426.12, 423.24, 429.08, 425.49, 425.36, 422.97, 422.24, 426.66, 417.92]
videos4 = [Basketball4, BQsquare4, RaceHorses4, BlowingBubbles4]


print("To 10 executions")
calcStatistics(videosseq, "Seq")
videosseq2 = extendList(videosseq)
print("")
speedup = averageSpeedUp(videosseq2, extendList(videos1))
calcStatistics(videos1, "1")
print("Average speedup for 1 Thread: "+str(speedup)+"\tefficiency: "+str(speedup/1)+"\n")

speedup = averageSpeedUp(videosseq2, extendList(videos2))
calcStatistics(videos2, "2")
print("Average speedup for 2 Thread: "+str(speedup)+"\tefficiency: "+str(speedup/2)+"\n")

speedup = averageSpeedUp(videosseq2, extendList(videos3))
calcStatistics(videos3, "3")
print("Average speedup for 3 Thread: "+str(speedup)+"\tefficiency: "+str(speedup/3)+"\n")

speedup = averageSpeedUp(videosseq2, extendList(videos4))
calcStatistics(videos4, "4")
print("Average speedup for 4 Thread: "+str(speedup)+"\tefficiency: "+str(speedup/4))


# Results for only one execution of 22 videos
# sequential
singleseq = [41867.93, 45647.75, 21049.56, 21107.65, 20444.62, 20837.24, 20491.81, 4348.55, 3952.60, 4281.86, 3932.83, 1034.25, 960.18, 999.07, 942.34, 8823.62, 9127.07, 9117.93, 3943.39, 7934.17, 9027.98, 8958.48]
# 1thread
sing1thread = [41883.04, 44191.17, 20408.66, 21131.32, 21115.33, 20520.10, 20488.72, 4290.47, 3953.29, 4226.90, 3981.17, 1059.60, 959.47, 972.59, 940.61, 8821.90, 9102.63, 8993.39, 4074.34, 8194.99, 9028.33, 8665.93]
# 2threads
sing2thread = [31815.54, 34445.93, 15581.57, 15832.74, 15555.52, 15317.74, 15889.84, 3284.70, 3016.35, 3226.37, 3055.20, 790.90, 731.15, 751.93, 727.24, 6815.21, 6686.03, 6838.95, 3065.70, 6167.64, 6624.75, 6692.11]
# 3threads
sing3thread = [22483.18, 24171.72, 11304.92, 11232.92, 11021.18, 11011.27, 11123.50, 2347.70, 2139.02, 2292.40, 2169.27, 574.66, 527.18, 526.77, 514.82, 4800.06, 4735.50, 4735.52, 2177.30, 4370.34, 4672.69, 4727.07]
# 4 threads
sing4thread = [17917.28, 19281.68, 9246.38, 8950.81, 9085.60, 8749.58, 8809.28, 1861.99, 1695.99, 1815.47, 1697.20, 456.97, 418.02, 435.56, 406.52, 3783.44, 3709.98, 3829.03, 1694.90, 3486.78, 3733.03, 3803.68]

calcStatistics(singleseq, "sing Seq")
print("")
speedup = averageSpeedUp(singleseq, sing1thread)
calcStatistics(sing1thread, "sing 1")
speedup2=speedUp(singleseq, sing1thread)
# print(speedup2)
print(max(speedup2))
print(min(speedup2))
print("Average speedup for 1 Thread: "+str(speedup)+"\tefficiency: "+str(speedup/1)+"\n")

speedup = averageSpeedUp(singleseq, sing2thread)
calcStatistics(sing2thread, "sing 2")
speedup2=speedUp(singleseq, sing2thread)
# print(speedup2)
print(max(speedup2))
print(min(speedup2))
print("Average speedup for 2 Thread: "+str(speedup)+"\tefficiency: "+str(speedup/2)+"\n")

speedup = averageSpeedUp(singleseq, sing3thread)
calcStatistics(sing3thread, "sing 3")
speedup2=speedUp(singleseq, sing3thread)
# print(speedup2)
print(max(speedup2))
print(min(speedup2))
print("Average speedup for 3 Thread: "+str(speedup)+"\tefficiency: "+str(speedup/3)+"\n")

speedup = averageSpeedUp(singleseq, sing4thread)
calcStatistics(sing4thread, "sing 4")
speedup2=speedUp(singleseq, sing4thread)
# print(speedup2)
print(max(speedup2))
print(min(speedup2))
print("Average speedup for 4 Thread: "+str(speedup)+"\tefficiency: "+str(speedup/4))

comeBack = [singleseq, sing1thread, sing2thread, sing3thread, sing4thread]
acelaration = [speedUp(singleseq, sing1thread),speedUp(singleseq, sing2thread),speedUp(singleseq, sing3thread),speedUp(singleseq, sing4thread)]
label = ["Sequential", "1 Thread", "2 Thread", "3 Thread", "4 Thread"]
label2 = ["1 Thread", "2 Thread", "3 Thread", "4 Thread"]
colors = ['r', 'c', 'g', 'm', 'gray']
colors2 = ['c', 'g', 'm', 'gray']
drawGraphics(comeBack, getXticks(), label, colors, "figure_1", "Video", "Time (s)")
drawGraphics(acelaration, getXticks(), label2, colors2, "speedUpchart", "Video", "speedup")
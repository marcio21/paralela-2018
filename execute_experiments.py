import re, os, shutil, datetime, time, subprocess, argparse
from subprocess import Popen,PIPE,STDOUT,call 
from time import gmtime, strftime, localtime

import numpy as np
import matplotlib.pyplot as plt
import matplotlib.patches as patches
import matplotlib.path as path


sourceFolderPath="HM-16.18"

# all 8 bits videos of CTC
configFiles=["Kimono", "BasketballDrive", "BQTerrace", "Cactus", "Johnny", "KristenAndSara", "SlideEditing", "SlideShow", "PeopleOnStreet", "Traffic", "BasketballPass", "BlowingBubbles", "BQSquare", "RaceHorses", "BasketballDrill", "BasketballDrillText", "BQMall", "PartyScene", "ChinaSpeed", "ParkScene", "FourPeople", "RaceHorsesC"]
videosList=["Kimono1_1920x1080_24hz.yuv", "BasketballDrive_1920x1080_50hz.yuv", "BQTerrace_1920x1080_60hz.yuv", "Cactus_1920x1080_50hz.yuv", "Johnny_1280x720_60hz.yuv", "KristenAndSara_1280x720_60hz.yuv", "SlideEditing_1280x720_30hz.yuv", "SlideShow_1280x720_20hz.yuv", "PeopleOnStreet_2560x1600_30hz_crop.yuv", "Traffic_2560x1600_30hz_crop.yuv", "BasketballPass_416x240_50hz.yuv", "BlowingBubbles_416x240_50hz.yuv", "BQSquare_416x240_60hz.yuv", "RaceHorses.yuv", "BasketballDrill_832x480_50hz.yuv", "BasketballDrillText_832x480_50hz.yuv", "BQMall_832x480_60hz.yuv", "PartyScene_832x480_50hz.yuv", "ChinaSpeed_1024x768_30hz.yuv", "ParkScene_1920x1080_24hz.yuv", "FourPeople_1280x720_60hz.yuv", "RaceHorses_832x480_30hz.yuv"]

# # test 1
# configFiles=["RaceHorses", "Kimono"]
# videosList=["RaceHorses.yuv", "Kimono1_1920x1080_24hz.yuv"]

# # test 2
# configFiles=["Kimono"]
# videosList=["Kimono1_1920x1080_24hz.yuv"]

# # test 3
# configFiles=["RaceHorses"]
# videosList=["RaceHorses.yuv"]

def showInScreen(content):
	print(content)

def saveInFile(fileName, content):
	f1=open(fileName, 'w')
	f1.write(content)
	f1.close()

def makeHEVC(action):
	os.chdir("build/linux/")
	os.system("make "+action)
	os.chdir("../../")

def createFolder(directory):
    try:
        if not os.path.exists(directory):
            os.makedirs(directory)
    except OSError:
        showInScreen('Error: Could not create ' +  directory + ' folder.')

def executeEncoder(configFiles, videosFolder, videosName, report, enableHWLoc, nThreads, mTime, nFrames):
	logFileName=report+"/report_"+configFiles
	if nFrames == 0:
		executeInst ="./bin/TAppEncoderStatic -c cfg/per-sequence/"+configFiles+".cfg -c cfg/encoder_lowdelay_main.cfg -i "+videosFolder+""+videosName +" > "+logFileName+".log"
	else:
		executeInst ="./bin/TAppEncoderStatic -c cfg/per-sequence/"+configFiles+".cfg --num_threads_fs="+str(nThreads)+" -f "+str(nFrames)+" -c cfg/encoder_lowdelay_main.cfg -i "+videosFolder+""+videosName +" > "+logFileName+".log"
	if mTime:
		executeInst ="/usr/bin/time -p -o "+logFileName+"_time.txt "+executeInst
	if enableHWLoc:
		if nThreads == 0:
			executeInst = "hwloc-bind core:0 -- "+ executeInst
		else:
			executeInst = "hwloc-bind core:0-"+str(nThreads - 1)+" -- "+ executeInst
	showInScreen(executeInst)
	output=(subprocess.Popen(executeInst, shell=True, stdout=subprocess.PIPE)).communicate()[0]


def makeFileBKP(report):
	try:
		shutil.make_archive(report+"_"+strftime("%d_%m_%Y-%I_%M_%S_backup", localtime()), "zip", ".", report)
	except Exception as e:
		showInScreen(e)

def executeExperiments(report, videosFolder, videosList, useHWLoc, nThreads, mTime, nFrames):
	for i in range(0, len(configFiles)):
		showInScreen("=================================")
		showInScreen("\t"+str(configFiles[i]))
		showInScreen("Started at "+strftime("%d/%m/%Y -- %I:%M:%S", localtime()))
		showInScreen("=================================")
		executeEncoder(configFiles[i], videosFolder, videosList[i], report, useHWLoc, nThreads, mTime, nFrames)


parser = argparse.ArgumentParser(description="Handle experiments with HM-16.18")
parser.add_argument("--clean", default=False, action="store_true" , help="Make clean")
parser.add_argument("--compile", default=False, action="store_true" , help="Compile the source code [make release]")
parser.add_argument("--execute", default=False, action="store_true" , help="Execute the binary")
parser.add_argument("--usehwloc", default=False, action="store_true" , help="Execute the binary using hwloc")
parser.add_argument("--makeBKP", default=False, action="store_true" , help="Make backup of experiments")
parser.add_argument("--time", default=False, action="store_true" , help="Measure the time using /usr/bin/time and save the output in file")
parser.add_argument('--nThreads', type=int, default='0', help="Define the number of threads that will be used to execute, also used to set the number of cores to be used by hwloc [core:0-nThreads]")
parser.add_argument('--expFolder', default='experimentFolder', nargs='?', help="Path to experiments folder")
parser.add_argument('--videoFolder', default=' ', nargs='?', help="Path to videos folder")
parser.add_argument('--nFrames', type=int, default='0', help="Define the number of frames to be encoded. 0 means use all the frames in video")
parser.add_argument("--collTimes", default=False, action="store_true" , help="Collect the Times of execution of experiments")

# parser.add_argument('--cfgFile', default='config.txt', nargs='?', help="Configuration file name")
args = parser.parse_args()

showInScreen("=================================")
showInScreen("\tStart executing")
showInScreen("Started at "+strftime("%d/%m/%Y -- %I:%M:%S", localtime()))
showInScreen("=================================")
showInScreen("\tParameters")
showInScreen("Clean:\t\t"+str(args.clean))
showInScreen("Compile:\t"+str(args.compile))
showInScreen("Execute:\t"+str(args.execute))
showInScreen("Usehwloc:\t"+str(args.usehwloc))
showInScreen("MakeBKP:\t"+str(args.makeBKP))
showInScreen("NThreads:\t"+str(args.nThreads))
showInScreen("Time:\t\t"+str(args.time))
showInScreen("ExpFolder:\t"+args.expFolder)
if args.videoFolder != ' ':
	showInScreen("VideoFolder:\t"+args.videoFolder)
showInScreen("NFrames:\t"+str(args.nFrames))
showInScreen("CollTimes:\t"+str(args.collTimes))
showInScreen("=================================")
os.chdir(sourceFolderPath)

report=args.expFolder
videosFolder='.'

if os.path.exists(args.videoFolder):
    videosFolder = args.videoFolder

if args.clean:
	showInScreen("\tCleaning the code")
	makeHEVC("clean")

if args.compile:
	showInScreen("=================================")
	showInScreen("\tCompiling the code")
	makeHEVC("release")

if args.execute:
	showInScreen("=================================")
	showInScreen("\tStart the experiments")
	createFolder(report)
	executeExperiments(report, videosFolder, videosList, args.usehwloc, args.nThreads, args.time, args.nFrames)

if args.makeBKP:
	showInScreen("=================================")
	showInScreen("\tMaking backup")
	makeFileBKP(report)
	

showInScreen("=================================")
showInScreen("\tFinished execution")
showInScreen("Ended at "+strftime("%d/%m/%Y -- %I:%M:%S", localtime()))
showInScreen("=================================")

# local execution
# reset && python execute_experiments.py --clean --compile --execute --usehwloc --makeBKP --nThreads 4 --time --expFolder parallel --videoFolder /home/user/source/videos
# reset && python execute_experiments.py --compile --execute --usehwloc --makeBKP --nThreads 4 --time --expFolder parallel --videoFolder /home/user/source/videos
# reset && python execute_experiments.py --clean --compile --execute --usehwloc --makeBKP --nThreads 8 --time --expFolder parallel --videoFolder /home/user/source/videos
# reset && python execute_experiments.py --compile --execute --usehwloc --makeBKP --time --expFolder sequential --videoFolder /home/user/source/videos --nFrames 64 
#!/bin/bash

# hmSource="HM-16.18"
# cd ${hmSource}/build/linux
# make
# cd ../../
# videosource="/media/marcio/archive/videos_sagi/"
# report="reports_full_search"
# configFiles="Kimono BasketballDrive BQTerrace Cactus Johnny KristenAndSara SlideEditing SlideShow PeopleOnStreet Traffic NebutaFestival_10bit SteamLocomotiveTrain_10bit BasketballPass BlowingBubbles BQSquare RaceHorses BasketballDrill BasketballDrillText BQMall PartyScene ChinaSpeed ParkScene FourPeople RaceHorsesC"
# videosList=("Kimono1_1920x1080_24hz.yuv" "BasketballDrive_1920x1080_50hz.yuv" "BQTerrace_1920x1080_60hz.yuv" "Cactus_1920x1080_50hz.yuv" "Johnny_1280x720_60hz.yuv" "KristenAndSara_1280x720_60hz.yuv" "SlideEditing_1280x720_30hz.yuv" "SlideShow_1280x720_20hz.yuv" "PeopleOnStreet_2560x1600_30hz_crop.yuv" "Traffic_2560x1600_30hz_crop.yuv" "NebutaFestival_2560x1600_60hz_10bit_crop.yuv" "SteamLocomotiveTrain_2560x1600_60hz_10bit_crop.yuv" "BasketballPass_416x240_50hz.yuv" "BlowingBubbles_416x240_50hz.yuv" "BQSquare_416x240_60hz.yuv" "RaceHorses.yuv" "BasketballDrill_832x480_50hz.yuv" "BasketballDrillText_832x480_50hz.yuv" "BQMall_832x480_60hz.yuv" "PartyScene_832x480_50hz.yuv" "ChinaSpeed_1024x768_30hz.yuv" "ParkScene_1920x1080_24hz.yuv" "FourPeople_1280x720_60hz.yuv" "RaceHorses_832x480_30hz.yuv")
# rm -r ${report}
# mkdir ${report}
# ls -la ${report}
# nb_concurrent_processes=1 #Usually use 8, but because there are only 4 we are using 4!!!
# j=0
# pos=0
# cd bin
# for cfgvd in ${configFiles}
# do
# 	./TAppEncoderStatic -c ../cfg/per-sequence/"${cfgvd}.cfg" -c ../cfg/encoder_lowdelay_main.cfg --FastSearch 1 -f 5 -i ${videosource}${videosList[${pos}]} >> ../${report}/"report_${cfgvd}.txt" &
# 	pos=$((pos + 1))
# 	j=$((j +1))
# 	((j == nb_concurrent_processes)) && { j=0; wait; }
# done

# nexecution="0 1 2 3 4 5 6 7 8 9"
# threads="1 2 3 4"
# for execNumb in ${nexecution}
# do
# 	for nThread in ${threads}
# 	do
# 		# echo "execution "${execNumb}" thread: "${nThread}
# 		# python find_pattern.py --compile --execute --usehwloc --makeBKP --time --expFolder parallel_${execNumb}_${nThread} --videoFolder /media/marcio/archive/videos_sagi/ --nFrames 64 --nThreads ${nThread}
# 	done
# done
# reset && 


nexecution="0 1 2 3 4 5 6 7 8 9"
for execNumb in ${nexecution}
do
	# echo "execution "${execNumb}" thread: "${nThread}
	# python find_pattern.py --compile --execute --usehwloc --makeBKP --time --expFolder parallel_${execNumb}_${nThread} --videoFolder /media/marcio/archive/videos_sagi/ --nFrames 64 --nThreads ${nThread}
	python find_pattern.py --compile --execute --usehwloc --makeBKP --time --expFolder sequential_${execNumb} --videoFolder /media/marcio/archive/videos_sagi/ --nFrames 64
done



# ./TAppEncoderStatic -c ../cfg/per-sequence/BQMall.cfg -c ../cfg/encoder_lowdelay_main.cfg -i /media/marcio/archive/videos_sagi/BQMall_832x480_60hz.yuv
# configFiles="BasketballDrill.cfg BasketballDrillText.cfg BasketballDrive.cfg BasketballPass.cfg BlowingBubbles.cfg BQMall.cfg BQSquare.cfg BQTerrace.cfg Cactus.cfg ChinaSpeed.cfg FourPeople.cfg Johnny.cfg Kimono.cfg KristenAndSara.cfg ParkScene.cfg PartyScene.cfg PeopleOnStreet.cfg RaceHorsesC.cfg RaceHorses.cfg SlideEditing.cfg SlideShow.cfg Traffic.cfg NebutaFestival_10bit.cfg SteamLocomotiveTrain_10bit.cfg"
# videosList=("BasketballDrill_832x480_50hz.yuv" "BasketballDrillText_832x480_50hz.yuv" "BasketballDrive_1920x1080_50hz.yuv" "BasketballPass_416x240_50hz.yuv" "BlowingBubbles_416x240_50hz.yuv" "BQMall_832x480_60hz.yuv" "BQSquare_416x240_60hz.yuv" "BQTerrace_1920x1080_60hz.yuv" "Cactus_1920x1080_50hz.yuv" "ChinaSpeed_1024x768_30hz.yuv" "FourPeople_1280x720_60hz.yuv" "Johnny_1280x720_60hz.yuv" "Kimono1_1920x1080_24hz.yuv" "KristenAndSara_1280x720_60hz.yuv" "ParkScene_1920x1080_24hz.yuv" "PartyScene_832x480_50hz.yuv" "PeopleOnStreet_2560x1600_30hz_crop.yuv" "RaceHorses_832x480_30hz.yuv" "RaceHorses.yuv" "SlideEditing_1280x720_30hz.yuv" "SlideShow_1280x720_20hz.yuv" "Traffic_2560x1600_30hz_crop.yuv" "NebutaFestival_2560x1600_60hz_10bit_crop.yuv" "SteamLocomotiveTrain_2560x1600_60hz_10bit_crop.yuv")

This software package is the reference software for Rec. ITU-T H.265 | ISO/IEC 23008-2 High efficiency video coding (HEVC). The reference software includes both encoder and decoder functionality.

Reference software is useful in aiding users of a video coding standard to establish and test conformance and interoperability, and to educate users and demonstrate the capabilities of the standard. For these purposes, this software is provided as an aid for the study and implementation of Rec. ITU-T H.265 | ISO/IEC 23008-2 High efficiency video coding.

The software has been jointly developed by the ITU-T Video Coding Experts Group (VCEG, Question 6 of ITU-T Study Group 16) and the ISO/IEC Moving Picture Experts Group (MPEG, Working Group 11 of Subcommittee 29 of ISO/IEC Joint Technical Committee 1).

A software manual, which contains usage instructions, can be found in the "doc" subdirectory of this software package.

Modified to support parallelism in Full search (xPatternSearch from class TEncSearch)

To compile in sequential mode the #define PARALLEL_FULLSEARCH (fileTypeDef.h) should be 0 otherwise allows the parallelism


usage: execute_experiments.py [-h] [--clean] [--compile] [--execute]
                              [--usehwloc] [--makeBKP] [--time]
                              [--nThreads NTHREADS] [--expFolder [EXPFOLDER]]
                              [--videoFolder [VIDEOFOLDER]]
                              [--nFrames NFRAMES] [--collTimes]

Handle experiments with HM-16.18

optional arguments:
  -h, --help            show this help message and exit
  --clean               Make clean
  --compile             Compile the source code [make release]
  --execute             Execute the binary
  --usehwloc            Execute the binary using hwloc
  --makeBKP             Make backup of experiments
  --time                Measure the time using /usr/bin/time and save the
                        output in file
  --nThreads NTHREADS   Define the number of threads that will be used to
                        execute, also used to set the number of cores to be
                        used by hwloc [core:0-nThreads]
  --expFolder [EXPFOLDER]
                        Path to experiments folder
  --videoFolder [VIDEOFOLDER]
                        Path to videos folder
  --nFrames NFRAMES     Define the number of frames to be encoded. 0 means use
                        all the frames in video
  --collTimes           Collect the Times of execution of experiments


Example of execution:

reset && python execute_experiments.py --clean --compile --execute --usehwloc --makeBKP --time --expFolder sequential --videoFolder /home/user/source/videos --nFrames 64 